{{ HTML::script('lib/jquery/jquery.min.js')}}

<div id="test"></div>

<script>
	$(function(){
		$.getJSON("parse/minorLeague.php", function(data, status, jqXHR){
			var minorLeagueTeamInfo=[];
			var leagueInfo=[];

			//MINOR LEAGUE TEAM DATA
			if(data.properties_nav.team_all.queryResults.row.length) {
				//foreach minor league team
				$.each(data.properties_nav.team_all.queryResults.row, function(i, row) {
					team = {};
					if ((row.team_id)&&(row.team_id!="")){
						row.sport_code_display = row.sport_code_display.replace(" Short-Season"," Short");
						// if (
						// 	(row.league_id!="121") && 
						// 	(row.league_id!="124") && 
						// 	(row.league_id!="125") && 
						// 	(row.league_id!="130") && 
						// 	(row.league_id!="134")

						// ){
							// team.team_id = row.team_id;
							// team.league_id = row.league_id;
							// team.mlb_org_id = row.mlb_org_id;
							// team.mlb_org_abbrev = row.mlb_org_abbrev;
							// team.city = row.city;
							// team.state = row.state;
							// team.sport_code = row.sport_code;
							// team.sport_code_display = row.sport_code_display;
							
							// team.name = row.name;
							// team.name_display_brief = row.name_display_brief;
							// team.active_sw = row.active_sw;

						if((row.division != "" && row.sport_code_display == "Major League Baseball") || (row.mlb_org_id != "" && row.mlb_org != ""))
						{

							team.name_display_full = row.name_display_full;
							team.league_full = row.league_full;
							team.division_full = row.division_full;
							team.sport_code_display = row.sport_code_display;
							team.city = row.city;
							team.state = row.state;
							team.first_year_of_play = row.first_year_of_play;
							team.mlb_org = row.mlb_org;

							minorLeagueTeamInfo.push(team);
						}
					}
				});
			}

			//MINOR LEAGUE DATA
			if(data.properties_nav.properties_league.queryResults.row.length) {
				//foreach minor league
				$.each(data.properties_nav.properties_league.queryResults.row, function(i, row) {
					league = {};
					if ((row.league_id)&&(row.league_id!="")){
						row.sport_code_display = row.sport_code_display.replace(" Short-Season"," Short");

						league.name_full = row.name_full;
						league.sport_code_display = row.sport_code_display;


						leagueInfo.push(league);
					}
				});
			}
			
			$.post("app/saveleagueinfo", {leagueInfo: JSON.stringify(leagueInfo), minorTeamInfo: JSON.stringify(minorLeagueTeamInfo)}, function(data){
				$("#test").html(data);
			});

			//$("#test").html(JSON.stringify(minorLeagueTeamInfo));
		});

		// $.getJSON("majorLeague.php", function(data, status, jqXHR){
		// 	var majorLeagueTeamInfo=[];
			
		// 	//MAJOR LEAGUE TEAM DATA
		// 	if(data.properties_nav.team_all.queryResults.row.length) {
		// 		//foreach minor league
		// 		$.each(data.properties_nav.team_all.queryResults.row, function(i, row) {
		// 			majorLeagueTeam = {};
		// 			if ((row.team_id)&&(row.team_id!="")){
		// 				if ((row.league_full == "American League" || row.league_full == "National League") && row.division_full != ""){
		// 					majorLeagueTeam.team_id = row.team_id;
		// 					majorLeagueTeam.league_id = row.league_id;
		// 					majorLeagueTeam.name_display_full = row.name_display_full;
		// 					majorLeagueTeam.league_full = row.league_full;
		// 					majorLeagueTeam.league_abbrev = row.league_abbrev;
		// 					majorLeagueTeam.division_full = row.division_full;
		// 					majorLeagueTeam.division_abbrev = row.division_abbrev;

		// 					majorLeagueTeamInfo.push(majorLeagueTeam);
		// 				}
		// 			}
		// 		});
		// 	}

		// 	// $.post("../dbInsert.php", {type: "major", majorLeagueTeamInfo: JSON.stringify(majorLeagueTeamInfo)}, function(data){
		// 	// 	alert(data);
		// 	// });
		// });
	});
</script>