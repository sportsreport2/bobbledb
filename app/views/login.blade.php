<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="description" content="">
          <meta name="author" content="">
          <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

          <title>Bobblehead Promotions</title>
          {{ HTML::style('lib/bootstrap/css/bootstrap.min.css')}}
          {{ HTML::style('lib/bootstrap/css/signin.css')}}
     </head>

     <body>
<div class="container">
               <div class="row"><div class="col-md-12"><h2 style="text-align:center;">Bobblehead Database</h2></div></div>
               <form class="form-signin" method="post" id="signinForm" action="{{URL::to('login')}}">
                    <h3 class="form-signin-heading" style="text-align: center">Sign In</h3>
                    <div class="alert alert-danger" id="errors"></div>
                    <input type="text" class="form-control" placeholder="Email" name="email" id="email" required autofocus>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
    <!--                 <label class="checkbox">
                         <input type="checkbox" value="remember" id="remember"> Remember me
                    </label> -->
                    <a href="{{URL::to('register')}}" class="checkbox" style="margin-left: -19px;">Sign Up</a>
                    <button class="btn btn-lg btn-primary btn-block" type="submit" id="signin">Sign in</button>
               </form>
          </div>

          <!-- Bootstrap core JavaScript
          ================================================== -->
          <!-- Placed at the end of the document so the pages load faster -->
          {{ HTML::script('lib/jquery/jquery.min.js')}}
          <script>
               $(function(){
                    $("#errors").hide();

                    $(".form-signin").keypress(function(e) {
                         if(e.which == 13) {
                              $("#signin").trigger('click');
                         }
                    });

                    $(document).on("click", "#signin", function(){
                         errors = "";
                         if($("#email").val() == "")
                              errors += "Email must have a value.<br>";

                         if($("#password").val() == "")
                              errors += "Password must have a value.<br>";

                         if(errors == ""){
                              $("#signinForm").submit();
                         }else{
                              $("#errors").html(errors).show();
                         }
                    });
               });
          </script>
     </body>
</html>