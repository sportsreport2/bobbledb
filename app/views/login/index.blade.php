<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="description" content="">
          <meta name="author" content="">
          <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

          <title>Bobblehead Promotions</title>

          <!-- Bootstrap core CSS -->
          <link href="view/lib/bootstrap/css/bootstrap.css" rel="stylesheet">

          <!-- Custom styles for this template -->
          <link href="view/css/signin.css" rel="stylesheet">
     </head>

     <body>
          <div class="container">
               <div class="row"><div class="col-md-12"><h2 style="text-align:center;">Bobblehead Promotions</h2></div></div>
               <form class="form-signin" method="post" id="signinForm">
                    <h3 class="form-signin-heading" style="text-align: center">Sign In</h3>
                    <div class="alert alert-danger" id="errors"></div>
                    <input type="text" class="form-control" placeholder="Username" name="username" id="username" required autofocus>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
    <!--                 <label class="checkbox">
                         <input type="checkbox" value="remember" id="remember"> Remember me
                    </label> -->
                    <a href="signup.php" class="checkbox" style="margin-left: -19px;">Request Access</a>
                    <button class="btn btn-lg btn-primary btn-block" type="submit" id="signin">Sign in</button>
               </form>
          </div> <!-- /container -->

          <!-- Bootstrap core JavaScript
          ================================================== -->
          <!-- Placed at the end of the document so the pages load faster -->
          <script type="text/javascript" src="view/lib/jquery/jquery.min.js"></script>
          <script>
               $(function(){
                    $("#errors").hide();

                    $(".form-signin").keypress(function(e) {
                         if(e.which == 13) {
                              $("#signin").trigger('click');
                         }
                    });

                    //$(document).on("click", "#signin", function(){
                    $("#signinForm").submit(function(){
                         var errors = "";
                         username = $("#username").val();
                         password = $("#password").val();
                         remember = $("#remember").val();

                         if(username == "")
                              errors += "Username must have a value.<br>";

                         if(password == "")
                              errors += "Password must have a value."

                         if(errors != ""){
                              $("#errors").html(errors).show();
                         }else{
                              $("#errors").hide();
                              $.post("jQueryPosts.php", {type: "signin", username: username, password: password, remember: remember}, function(data){
                                   if(data == "error")
                                        $("#errors").html("Invalid Username/Password.").show();
                                   else if(data == "loggedIn")
                                        window.location = "view/";
                              });
                         }
                         return false;
                    });
               });
          </script>
     </body>
</html>