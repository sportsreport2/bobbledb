@extends('layouts.main')

@section('nav')
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bobblehead Database</a>
     </div>

     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
          <li><a href="home">Database</a></li>
          <li><a href="list">List&nbsp;<span class="badge pull-right" id="listBadge"></span></a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
          <?php
          if (Entrust::hasRole('Admin')){
               echo '
               <li class="active"><a href="#">Add Entry</a></li>';
          }
          ?>
          <li>
          <form class="navbar-form navbar-right" method="get" action="{{URL::to('logout')}}">
          <button type="submit" class="btn btn-primary">Sign Out</button>
          </form>
          </li>
     </ul>
     </div><!-- /.navbar-collapse -->
</nav>
@stop

@section('content')
     <div id="wrap">
          <div class="container-fluid">
               <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                         <h3 class="text-center">Database Entry</h3>
                             @if(Session::get("responseType"))
                                 <div class="alert alert-success">
                                     <button type="button" class="close" data-dismiss="alert">&times;</button>
                                     {{Session::get("responseType")}}
                                 </div>
                             @endif
                         <ul>
                              @foreach($errors->all() as $error)
                                   <li>{{ $error }}</li>
                              @endforeach
                         </ul>
                         <div class="panel panel-default">
                              <div class="panel-body">               
                                   <div class="alert alert-danger" id="errors">
                                        @foreach($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                        @endforeach
                                   </div>
                                   <div class="alert alert-success" id="success" style="text-align: center;">
                                        Successfully requested access!<br>
                                        In no later than a week you will receive an email confirming or rejecting your access.
                                        <a href="{{URL::to('index')}}">Return Home</a>
                                   </div>
                                   
                                   {{Form::open(array('method' => 'post', 'files' => true, 'class' => 'form form-signup', 'role' => 'form', 'id' => 'entryForm'))}}
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                  <input type="text" class="form-control" placeholder="Item Name" id="name" name="name" value="<?php echo ((isset($itemName))? $itemName : ""); ?>" autofocus />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-list"></span></span>
                                                  <input type="text" class="form-control" placeholder="Type" id="type" name="type" value="<?php echo ((isset($type))? $type : ""); ?>" />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
                                                  <textarea class="form-control" rows="3" placeholder="Description" name="description" id="description"><?php echo ((isset($description))? $description : ""); ?></textarea>
                                             </div>
                                        </div>
                                        <!-- <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                  <input type="text" class="form-control" placeholder="Team" id="team" name="team" />
                                                  <div class="input-group-btn">
                                                       <button type="button" class="btn btn-default btn-md pull-right" id="addTeam">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                       </button>
                                                  </div>
                                             </div>
                                        </div> -->
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                  <select class="form-control" name="team">
                                                       @for($i=0; $i<count($teams); $i++)
                                                            <option value="{{$teams[$i][0]}}" <?php echo ((isset($teamID) && $teams[$i][0] == $teamID)? "selected" : ""); ?>>{{$teams[$i][1]}}</option>
                                                       @endfor
                                                  </select>
                                                  <!-- <div class="input-group-btn">
                                                       <button type="button" class="btn btn-default btn-md pull-right" id="addTeam">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                       </button>
                                                  </div> -->
                                             </div>
                                        </div>

                                        <div class="panel panel-default" id="newTeamPanel">
                                             <div class="panel-heading">Add New Team</div>
                                             <div class="panel-body">
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="Team Name" id="newTeamName" name="newTeamName"  />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="League" id="newTeamLeague" name="newTeamLeague" />
                                                            <div class="input-group-btn">
                                                                 <button type="button" class="btn btn-default btn-md pull-right" id="addLeague">
                                                                      <span class="glyphicon glyphicon-plus"></span>
                                                                 </button>
                                                            </div>
                                                       </div>
                                                  </div>

                                                  <div class="panel panel-default" id="newLeaguePanel">
                                                       <div class="panel-heading">Add New League</div>
                                                       <div class="panel-body">
                                                            <div class="form-group">
                                                                 <div class="input-group">
                                                                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                                      <input type="text" class="form-control" placeholder="League Name" id="newLeagueName" name="newLeagueName" />
                                                                 </div>
                                                            </div>
                                                            <div class="form-group">
                                                                 <div class="input-group">
                                                                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                                      <input type="text" class="form-control" placeholder="Sport Code Eg. Triple-A" id="newLeagueSportcode" name="newLeagueSportcode" />
                                                                 </div>
                                                            </div>
                                                            <button class="btn btn-danger btn-block" role="button" id="newLeagueCancel" type="button">CANCEL</button>
                                                       </div>
                                                  </div>

                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="Division" id="newTeamDivision" name="newTeamDivision" />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="Sport Code Eg. Triple-A" id="newTeamSportcode" name="newTeamSportcode" />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="City" id="newTeamCity" name="newTeamCity" />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="State" id="newTeamState" name="newTeamState" />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="First Year Of Play" id="newTeamFirstYearOfPlay" name="newTeamFirstYearOfPlay" />
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                            <input type="text" class="form-control" placeholder="MLB Affiliation" id="newTeamAffiliation" name="newTeamAffiliation" />
                                                       </div>
                                                  </div>
                                                  <button class="btn btn-danger btn-block" role="button" id="newTeamCancel" type="button">CANCEL</button>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
                                                  <input type="text" class="form-control" placeholder="Manufacturer" id="manufacturer" name="manufacturer" value="<?php echo ((isset($manufacturer))? $manufacturer : ""); ?>" />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                  <input type="text" class="form-control" placeholder="Giveaway Date" id="giveawayDate" name="giveawayDate" value="<?php echo ((isset($giveawayDate))? $giveawayDate : ""); ?>" />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="">#</span></span>
                                                  <input type="number" class="form-control" placeholder="Quantity" id="quantity" name="quantity" value="<?php echo ((isset($quantity))? $quantity : ""); ?>" />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <div class="input-group">
                                                  <span class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></span>
                                                  <input type="file" class="form-control" id="picture" name="picture" />
                                             </div>
                                        </div>
                                        <button class="btn btn-primary btn-block" role="button" id="submitBtn" type="button">SUBMIT</button>
                                        @if(isset($itemID))
                                             <input type="hidden" name="itemID" value="{{$itemID}}" />
                                        @endif
                                   </form>
                              </div>  
                         </div>
                    </div>
               </div>
          </div>
     </div>
@stop

@section('pageJS')
     {{ HTML::script('lib/jquery/jquery.min.js')}}
     {{ HTML::script('lib/jqueryui/js/jquery-ui-1.10.3.custom.min.js')}}

     <script>
          $(function(){
               $("#errors, #success, #newTeamPanel, #newLeaguePanel").hide();

               $("#giveawayDate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: "yy-mm-dd" 
               });

               $( "#team" ).autocomplete({
                    source: function (request, response) {
                         $.ajax({
                              url: "teams",
                              type: "GET",
                              data: request,
                              dataType: "json",
                              success: function (data) {
                                   response($.map(data, function (el) {
                                        return {
                                             label: el.label,
                                             value: el.id
                                        };
                                   }));
                              }
                         });
                    },
               });

               $(".form-signup").keypress(function(e) {
                    if(e.which == 13) {
                         $("#submitBtn").trigger('click');
                    }
               });

               $(document).on("click", "#submitBtn", function(){
                    errors = "";
                    if($("#name").val() == "")
                         errors += "Item Name must have a value.<br>";

                    if($("#description").val() == "")
                         errors += "Description must have a value.<br>";

                    if($("#team").val() == "")
                         errors += "Team must have a value.<br>";

                    if($("#type").val() == "")
                         errors += "Type must have a value.<br>";

                    if($("#manufacturer").val() == "")
                         errors += "Manufacturer must have a value.<br>";

                    if($("#giveawayDate").val() == "")
                         errors += "Giveaway Date must have a value.<br>";

                    if($("#quantity").val() == "")
                         errors += "Quantity password must have a value.<br>";

                    if(errors == ""){
                         $("#entryForm").submit();
                    }else{
                         $("#success").hide();
                         $("#errors").html(errors).show();
                    }
               });

               $(document).on("click", "#addTeam", function(){
                    $("#newTeamPanel").slideDown();
               });

               $(document).on("click", "#newTeamCancel", function(){
                    $("#newTeamPanel").slideUp();
               });

               $(document).on("click", "#addLeague", function(){
                    $("#newLeaguePanel").slideDown();
               });

               $(document).on("click", "#newLeagueCancel", function(){
                    $("#newLeaguePanel").slideUp();
               });
          });
     </script>
@stop   