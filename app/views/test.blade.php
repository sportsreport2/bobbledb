@extends('layouts.main')

@section('nav')
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">[NAME]</a>
     </div>

     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
          <li class="active"><a href="#">Database</a></li>
          <li><a href="list">List&nbsp;<span class="badge pull-right" id="listBadge"></span></a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
          <?php
          if (Entrust::hasRole('Admin')){
               echo '
               <li><a href="entry">Add Entry</a></li>';
          }
          ?>
          <li>
          <form class="navbar-form navbar-right" method="get" action="{{URL::to('logout')}}">
          <button type="submit" class="btn btn-primary">Sign Out</button>
          </form>
          </li>
     </ul>
     </div><!-- /.navbar-collapse -->
</nav>
@stop

@section('content')
     <div id="wrap">
          <div class="container-fluid">
               <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 col-xs-12">
                         <table id="example" class="table table-bordered table-striped">
                              <thead>
                                   <tr>
                                        <th data-class="expand" nowrap="nowrap">List</th>
                                        <th>Name</th>
                                        <th>Team</th>
                                        <th>Type</th>
                                        <th data-hide="phone,tablet">Description</th>
                                        <th nowrap="nowrap" data-hide="phone,tablet">Manufacturer</th>
                                        <th nowrap="nowrap">Date</th>
                                        <th>Quantity</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   @for($i=0; $i<count($items); $i++)
                                        <tr>
                                             <td></td>
                                             <td>{{$items[$i][0]}}</td>
                                             <td>{{$items[$i][1]}}</td>
                                             <td>{{$items[$i][2]}}</td>
                                             <td>{{$items[$i][3]}}</td>
                                             <td>{{$items[$i][4]}}</td>
                                             <td>{{$items[$i][5]}}</td>
                                             <td>{{$items[$i][6]}}</td>
                                        </tr>4
                                   @endfor
                              </tbody>
                         </table>
                    </div>
                    <div class="col-md-1"></div>
               </div>
          </div>
     </div>
@stop

@section('pageJS')
     <script src="{{URL::asset('lib/jquery/jquery.min.js')}}"></script>
     <script src="{{URL::asset('lib/jquery.dataTables/js/jquery.dataTables.min.js')}}"></script>

     <script>
          $(function(){
               $("#example").dataTable();
          });
     </script>
@stop   