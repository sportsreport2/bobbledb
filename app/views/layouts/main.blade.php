<!doctype html>
<html lang="en-US">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <title>Bobblehead Database</title>

	    {{ HTML::style('lib/jqueryui/css/smoothness/jquery-ui-1.10.3.custom.min.css')}}
	    {{ HTML::style('lib/bootstrap/css/bootstrap.min.css')}}
		{{ HTML::style('lib/jquery.dataTables/css/jquery.dataTables.min.css')}}

	    <style>
			.title {
				font-size: larger;
				font-weight: bold;
			}

			.table th.centered-cell, .table td.centered-cell {
				text-align: center;
			}

			html,
			body {
				height: 100%;
			/* The html and body elements cannot have any padding or margin. */
			}

			/* Wrapper for page content to push down footer */
			#wrap {
				min-height: 2000px;
				padding-top: 70px;
			}

			.dataTables_info#example_info{
				text-align: left;
			}

			.disabledLink{
				pointer-events: none;
				cursor: default;
			}

			.bottom-buffer{
				margin-bottom: 20px;
			}

	    </style>
	</head>

	<body>
		@yield('nav')
		@yield('content')
		@yield('pageJS')
	</body>
</html>