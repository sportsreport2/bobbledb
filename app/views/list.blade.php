@extends('layouts.main')

@section('nav')
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bobblehead Database</a>
     </div>

     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
          <li><a href="home">Database</a></li>
          <li class="active"><a href="#">List&nbsp;<span class="badge pull-right" id="listBadge"></span></a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
          <?php
          if (Entrust::hasRole('Admin')){
               echo '
               <li><a href="entry">Add Entry</a></li>';
          }
          ?>
          <li>
          <form class="navbar-form navbar-right" method="get" action="{{URL::to('logout')}}">
          <button type="submit" class="btn btn-primary">Sign Out</button>
          </form>
          </li>
     </ul>
     </div><!-- /.navbar-collapse -->
</nav>
@stop

@section('content')
     <div id="wrap">
          <div class="container-fluid">
               <div class="row">
                    <div class="col-lg-1 col-md-1"></div>
                    <div class="col-lg-1 col-md-1">
                         <div class="btn-toolbar" role="toolbar">
                              <div class="btn-group">
                                   <button type="button" class="btn btn-danger" id="reset">Reset</button>
                              </div>
                         </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-xs-12 bottom-buffer">
                         <form class="form-inline" role="form">
                              <div class="form-group">
                                   <input type="text" class="form-control" id="fromDate" name="fromDate" placeholder="From Date" />
                                   <input type="text" class="form-control" id="toDate" name="toDate" placeholder="To Date" />
                              </div>
                         </form>
                    </div>
                    <div class="col-lg-5 col-md-5 col-xs-12 bottom-buffer">
                         <div class="input-group">
                              <div class="input-group-btn">
                                   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="search">All <span class="caret"></span></button>
                                   <ul class="dropdown-menu">
                                        <li class="search"><a href="#">All</a></li>
                                        <li class="search"><a href="#">Name</a></li>
                                        <li class="search"><a href="#">Team</a></li>
                                        <li class="search"><a href="#">Type</a></li>
                                        <li class="search"><a href="#">Description</a></li>
                                        <li class="search"><a href="#">Manufacturer</a></li>
                                   </ul>

                              </div><!-- /btn-group -->
                              <input type="text" class="form-control" id="searchText">
                              <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                         </div><!-- /input-group -->
                    </div>
                    <div class="col-lg-1"></div>
                    <br><br>
               </div>
          </div>     

          <div class="container-fluid">
               <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 col-xs-12">
                         <table id="example" class="table table-bordered table-striped">
                              <thead>
                                   <tr>
                                        <th data-class="expand" nowrap="nowrap">List</th>
                                        <th>Name</th>
                                        <th>Team</th>
                                        <th>Type</th>
                                        <th data-hide="phone,tablet">Description</th>
                                        <th nowrap="nowrap" data-hide="phone,tablet">Manufacturer</th>
                                        <th nowrap="nowrap">Date</th>
                                        <th>Quantity</th>
                                        <th>Picture</th>
                                   </tr>
                              </thead>
                              <tbody>

                              </tbody>
                         </table>
                    </div>
                    <div class="col-md-1"></div>
               </div>
          </div>
     </div>
@stop

@section('pageJS')
     <script src="{{URL::asset('lib/jquery/jquery.min.js')}}"></script>
     <script src="{{URL::asset('lib/bootstrap/js/bootstrap.js')}}"></script>
     <script src="{{URL::asset('lib/jqueryui/js/jquery-ui-1.10.3.custom.min.js')}}"></script>
     <script src="{{URL::asset('lib/jquery.dataTables/js/jquery.dataTables.min.js')}}"></script>
     <script src="{{URL::asset('lib/jquery.dataTables/js/DT_bootstrap.js')}}"></script>
     <script src="{{URL::asset('lib/js/list.js')}}"></script>
     <script src="{{URL::asset('lib/js/view.js')}}"></script>
     <script src="{{URL::asset('lib/js/functions.js')}}"></script>

     <script>
          $(function(){
               ///////BADGE PROCESS///////
               $.get("listfunctions", {type: "markBadgesSeen"});

               ///////LIST PROCESS///////
               $(document).on("click", ".list", function(){
                    $.get("listfunctions", {type: "deleteList", itemID: $(this).attr("id")}, function(data){
                         storeParams();
                    });
               });
          });
     </script>
@stop   