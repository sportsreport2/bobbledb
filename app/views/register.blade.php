<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Bobblehead Promotions</title>
<!--     <link rel="stylesheet" type="text/css" href="view/jqueryui/css/smoothness/jquery-ui-1.10.3.custom.min.css"/>
    <link rel="stylesheet" type="text/css" href="view/lib/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="view/lib/bootstrap/css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" type="text/css" href="view/lib/jquery.dataTables/css/DT_bootstrap.css"/>
    <link rel="stylesheet" href="files/css/datatables.responsive.css"/>
    <link href="view/css/signin.css" rel="stylesheet"> -->
    {{ HTML::style('lib/bootstrap/css/bootstrap.min.css')}}
</head>
<body>

<div id="wrap">
     <div class="container-fluid">
          <div class="row"><div class="col-md-12"><h2 style="text-align:center;">Bobblehead Promotions</h2></div></div>
          <div class="row">
               <div class="col-md-4 col-md-offset-4">
                    <h3 class="text-center">Sign Up</h3>
                    <div><a href="{{URL::to('index')}}">Return Home</a></div>
                    <ul>
                         @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                         @endforeach
                   </ul>
                    <div class="panel panel-default">
                         <div class="panel-body">               
                              <div class="alert alert-danger" id="errors">
                                   @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                   @endforeach
                              </div>
                              <div class="alert alert-success" id="success" style="text-align: center;">
                                   Successfully requested access!<br>
                                   In no later than a week you will receive an email confirming or rejecting your access.
                                   <a href="{{URL::to('index')}}">Return Home</a>
                              </div>
                              <form class="form form-signup" role="form" id="registerForm" method="post">
                                   <div class="form-group">
                                       <div class="input-group">
                                             <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                                             </span>
                                             <input type="text" class="form-control" placeholder="First Name" id="first" name="firstname" autofocus />
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Last Name" id="last" name="lastname" />
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Email Address" id="email" name="email" />
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span>
                                                </span>
                                                <input type="password" class="form-control" placeholder="Password" id="password" name="password"  />
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span>
                                                </span>
                                                <input type="password" class="form-control" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation" />
                                       </div>
                                   </div>
                                   <button class="btn btn-primary btn-block" role="button" id="submitBtn" type="button">SUBMIT</button>
                              </form>
                         </div>  
                    </div>
               </div>
          </div>
     </div>
</div> 

{{ HTML::script('lib/jquery/jquery.min.js')}}
<script>
     $(function(){
          $("#errors, #success").hide();
          $("#registerForm").attr("autocomplete", "off");

          $(".form-signup").keypress(function(e) {
               if(e.which == 13) {
                    $("#submitBtn").trigger('click');
               }
          });

          $(document).on("click", "#submitBtn", function(){
               errors = "";
               if($("#first").val() == "")
                    errors += "First Name must have a value.<br>";

               if($("#last").val() == "")
                    errors += "Last Name must have a value.<br>";

               if($("#email").val() == "")
                    errors += "Email must have a value.<br>";

               if($("#password").val() == "")
                    errors += "Password must have a value.<br>";

               if($("#password_confirmation").val() == "")
                    errors += "Confirm password must have a value.<br>";

               if(errors == ""){
                    $("#registerForm").submit();
               }else{
                    $("#success").hide();
                    $("#errors").html(errors).show();
               }
          });
     });
</script>
</body>
</html>
