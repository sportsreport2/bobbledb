<?php

class Home extends Eloquent 
{
	public static function getItems()
	{
		//$getItemsSQL = DB::connection("bobbledb")->select("SELECT name, teamID, type, description, manufacturer, giveawayDate, quantity FROM item");

		$output = array("sql"=> "", "sEcho"=> Input::get("sEcho"), "iTotalRecords"=> "", "iTotalDisplayRecords"=> "", "aaData"=> array());
		$date = $search = "";

		///////DATE LOGIC///////
		if(Input::has('specificDate'))
		     $date = "AND giveawayDate='".Input::get('specificDate')."'";
		else if(Input::has('dateStart') && Input::has('dateEnd')){
		     $date = "AND giveawayDate BETWEEN '".Input::get('dateStart')."' AND '".Input::get('dateEnd')."'";
		}

		///////SEARCH LOGIC///////
		if(Input::has("type")){
		     switch(Input::get("type")){
		          case "All":
		               $search = "AND (i.name LIKE '%".Input::get('term')."%' OR t.name LIKE '%".Input::get('term')."%' OR type LIKE '%".Input::get('term')."%' OR description LIKE '%".Input::get('term')."%' OR manufacturer LIKE '%".Input::get('term')."%') ";
		          break;

		          case "Name":
		               $search = "AND i.name LIKE '%".Input::get('term')."%'";
		          break;

		          case "Team":
		               $search = "AND t.name LIKE '%".Input::get('term')."%'";
		          break;

		          case "Type":
		               $search = "AND type LIKE '%".Input::get('term')."%'";
		          break;

		          case "Description":
		               $search = "AND description LIKE '%".Input::get('term')."%'";
		          break;

		          case "Manufacturer":
		               $search = "AND manufacturer LIKE '%".Input::get('term')."%'";
		          break;

		          default:
		               $search = "";
		          break;
		     }
		}

		$columns = array('', 'itemName', 'teamName', 'type', 'description', 'manufacturer', 'giveawayDate', 'quantity', '');

	     $sOrder = "";
	     if ( Input::has('iSortCol_0') )
	     {
	          $sOrder = "ORDER BY  ";
	          for ( $i=0 ; $i<intval( Input::get('iSortingCols') ) ; $i++ )
	          {
	               if ( Input::get( 'bSortable_'.intval(Input::get('iSortCol_'.$i)) ) == "true")
	               {
	                    if($columns[ intval( Input::get('iSortCol_'.$i) ) ] != "")
	                         $sOrder .= $columns[ intval( Input::get('iSortCol_'.$i) ) ]." ".mysql_real_escape_string( Input::get('sSortDir_'.$i) ) .", ";
	               }
	          }
	          
	          $sOrder = substr_replace( $sOrder, "", -2 );
	          if ( $sOrder == "ORDER BY" )
	          {
	               $sOrder = "ORDER BY giveawayDate";
	          }
	     }

	     $output["sql"] = "SELECT SQL_CALC_FOUND_ROWS t.name as teamName, i.name as itemName, type, description, manufacturer, giveawayDate, quantity, itemID, pictureExt FROM item i, team t WHERE i.teamID=t.teamID AND deleted='0' ".$date." ".$search." ".$sOrder." ";

		$sql = DB::connection("bobbledb")->select("SELECT SQL_CALC_FOUND_ROWS t.name as teamName, i.name as itemName, type, description, manufacturer, giveawayDate, quantity, itemID, pictureExt FROM item i, team t WHERE i.teamID=t.teamID AND deleted='0' ".$date." ".$search." ".$sOrder." ");

		$totalItems = DB::connection("bobbledb")->select("SELECT COUNT(itemID) as items FROM item");

		$output['iTotalRecords'] = $totalItems[0]->items;
		$output['iTotalDisplayRecords'] = $totalItems[0]->items;

		for($i=0; $i<count($sql); $i++)
		{
			$row = array();

			$listSQL = DB::connection("bobbledb")->select("SELECT listID FROM list WHERE itemID='".$sql[$i]->itemID."' AND userID='".Auth::user()->id."' AND deleted='0' ");
			if($listSQL)
				$class = "btn-success";
			else
				$class = "btn-default";

			$row[] = '<button type="button" class="btn '.$class.' btn-xs list" id="'.$sql[$i]->itemID.'">
		                 	<span class="glyphicon glyphicon-ok"></span>
		               </button>';
			$row[] = $sql[$i]->itemName;
			$row[] = $sql[$i]->teamName;
			$row[] = $sql[$i]->type;
			$row[] = $sql[$i]->description;
			$row[] = $sql[$i]->manufacturer;
			$row[] = $sql[$i]->giveawayDate;
			$row[] = $sql[$i]->quantity;
			$row[] = '<img src="../uploads/'.$sql[$i]->itemID.'.'.$sql[$i]->pictureExt.'" alt="'.$sql[$i]->itemName.'" height="200" width="200" />';
			
          	if (Entrust::hasRole('Admin')){
				$row[] = '<td>
						<form method="get" action="'.URL::route('edit').'">
							<input type="hidden" name="id" value="'.$sql[$i]->itemID.'" />
							<button type="submit" class="btn btn-info btn-xs edit" id="'.$sql[$i]->itemID.'">Edit</button>					
						</form>
						<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-xs delete" id="'.$sql[$i]->itemID.'">Delete</button>
					</td>';
			}

			$output['aaData'][] = $row;
 		}

 		return $output;
	}

	public static function addList()
	{
		$checkSQL = DB::connection("bobbledb")->select("SELECT listID FROM list WHERE itemID='".Input::get("itemID")."' AND userID='".Auth::user()->id."' ");
		if(count($checkSQL) == 1)
		{
			$updateSQL = DB::connection("bobbledb")->table('list')->where('listID', $checkSQL[0]->listID)->update(array('deleted' => 0));
			if($updateSQL)
				return true;
		}
		else
		{
			$insertSQL = DB::connection("bobbledb")->table("list")->insert(array('itemID' => Input::get("itemID"), 'userID' => Auth::user()->id));
			if($insertSQL)
				return true;
		}
	}

	public static function deleteList()
	{
		$checkSQL = DB::connection("bobbledb")->select("SELECT listID FROM list WHERE itemID='".Input::get("itemID")."' AND userID='".Auth::user()->id."' ");
		if(count($checkSQL) == 1)
		{
			$deleteSQL = DB::connection("bobbledb")->table('list')->where('listID', $checkSQL[0]->listID)->update(array('deleted' => 1));
			if($deleteSQL)
				return true;
		}
	}

	public static function listBadge()
	{
		$selectSQL = DB::connection("bobbledb")->select("SELECT listID FROM list WHERE userID='".Auth::user()->id."' AND deleted='0' AND seen='0' ");
		if($selectSQL)
			return count($selectSQL);
	}

	public static function markBadgesSeen()
	{
		$updateSQL = DB::connection("bobbledb")->table('list')->where('userID', Auth::user()->id)->update(array('seen' => 1));
			if($updateSQL)
				return true;
	}

	public static function deleteItem()
	{
		$deleteSQL = DB::connection("bobbledb")->table('item')->where('itemID', Input::get("id"))->update(array('deleted' => 1));
		if($deleteSQL)
			return true;
	}



}