<?php

class GlobalModel
{
	/**
	* This function converts the array of objects returned by a database select into an array. 
	* Ex. before: array([0]=>stdClass([key]=>value)) 
	* Ex. after:  array([0]=>array([0]=>value))
	*	
	* @param Array $DBObjectsArray contains an array of DB objects 
	* @return Array Converts the array of DB objects into a normally indexed multidimensional array.
	*/
	public static function convertDBObjectsToArray($DBObjectsArray)
	{
		$returnArray = array();
		foreach($DBObjectsArray as $row)
		{
			//use the following to convert array of objects into an associative array
				//$returnArray[] = json_decode(json_encode($row), true);

			$numVars = count(get_object_vars($row));
			$rowArray = array();
			foreach($row as $key=>$value)
				$rowArray[] = $value;
			$returnArray[] = $rowArray;
		}
		return $returnArray;
	}

}

