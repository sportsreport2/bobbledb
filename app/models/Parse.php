<?php

class Parse extends Eloquent  
{
	public static function saveLeagueInfo($leagueInfo)
	{
		for($i=0; $i<count($leagueInfo); $i++)
		{
			$saveLeagueInfoQuery = DB::connection("bobbledb")->table('league')->insert(array('name'=>$leagueInfo[$i]['name_full'], 'sportCode'=>$leagueInfo[$i]['sport_code_display'], 'active'=>true, 'created_at'=>DB::raw('UNIX_TIMESTAMP(NOW())')));
			if(!$saveLeagueInfoQuery)
				return false;
		}
		return true;
	}

	public static function saveMinorTeamInfo($minorTeamInfo)
	{
		for($i=0; $i<count($minorTeamInfo); $i++)
		{
			$getLeagueQuery = DB::connection("bobbledb")->select("SELECT leagueID, name FROM league WHERE name='".$minorTeamInfo[$i]['league_full']."' ");

			if(count($getLeagueQuery) >= 1)
				$league = $getLeagueQuery[0]->leagueID;
			else
				$league = 0;

			$affiliation = 0; 
			if($minorTeamInfo[$i]['mlb_org'] != "")
			{
				$getMLBQuery = DB::connection("bobbledb")->select("SELECT teamID FROM team WHERE name='".$minorTeamInfo[$i]['mlb_org']."' ");
				if(count($getMLBQuery) >= 1)
					$affiliation = $getMLBQuery[0]->teamID;
			}

			$saveMinorTeamInfoQuery = DB::connection("bobbledb")->table('team')->insert(array('leagueID'=> $league, 'name'=>$minorTeamInfo[$i]['name_display_full'], 'division'=>$minorTeamInfo[$i]['division_full'], 'sportCode'=>$minorTeamInfo[$i]['sport_code_display'], 'city'=>$minorTeamInfo[$i]['city'], 'state'=>$minorTeamInfo[$i]['state'], 'firstYearOfPlay'=>$minorTeamInfo[$i]['first_year_of_play'], 'affiliation'=>$affiliation));
			if(!$saveMinorTeamInfoQuery)
				return false;
		}
		return true;
	}
}