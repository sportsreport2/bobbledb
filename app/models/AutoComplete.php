<?php

class AutoComplete 
{
	public static function getTeams($term)
	{
		$getTeamsQuery = DB::connection("bobbledb")->select("SELECT DISTINCT name, teamID FROM team");

		if($getTeamsQuery >= 1)
			$teamsArray = $getTeamsQuery;

		$teams = array();
		for($i=0; $i<count($teamsArray); $i++)
		{
			$teams[$teamsArray[$i]->name] = $teamsArray[$i]->teamID;
		}

		$result = array();
		foreach ($teams as $key=>$value) {
			if (strpos(strtolower($key), $term) !== false) {
				array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key)));
			}
			if (count($result) > 11)
				break;
		}

		return json_encode($result);
	}

}

