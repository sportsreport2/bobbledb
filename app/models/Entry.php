<?php

class Entry extends Eloquent 
{

	public static function saveEntryInfo($teamID, $name, $type, $description, $manufacturer, $giveawayDate, $quantity, $picture)
	{
		$ext = $picture->getClientOriginalExtension();
		$insertEntryNum = DB::connection("bobbledb")->table("item")->insertGetId(array('teamID' => $teamID, 'name' => $name, 'type' => $type, 'description' => $description, 'manufacturer' => $manufacturer, 'giveawayDate' => $giveawayDate, 'quantity' => $quantity, 'pictureExt' => $ext, 'createdBy' => Auth::user()->id));		

		if($picture->move("/var/www/bobbledb/public/uploads/", $insertEntryNum.'.'.$ext))
			return true;
	}

	public static function getTeams()
	{
		$getTeamsSQL = DB::connection("bobbledb")->select("SELECT teamID, name FROM team ORDER BY name");

		return GlobalModel::convertDBObjectsToArray($getTeamsSQL);
	}

	public static function editData()
	{
		$editSQL = DB::connection("bobbledb")->select("SELECT name as itemName, type, description, teamID, manufacturer, giveawayDate, quantity, itemID, pictureExt FROM item WHERE deleted='0' AND itemID='".Input::get("id")."' ");

		return $editSQL;
	}

	public static function saveEdit($itemID, $teamID, $name, $type, $description, $manufacturer, $giveawayDate, $quantity, $picture)
	{
		if(Input::has("picture")){
			$ext = $picture->getClientOriginalExtension();
			$updateSQL = DB::connection("bobbledb")->table('item')->where('itemID', $itemID)->update(array('teamID' => $teamID, 'name' => $name, 'type' => $type, 'description' => $description, 'manufacturer' => $manufacturer, 'giveawayDate' => $giveawayDate, 'quantity' => $quantity, 'pictureExt' => $ext));

			if($picture->move("/var/www/bobbledb/public/uploads/", $itemID.'.'.$ext))
				return true;
		}else{
			$updateSQL = DB::connection("bobbledb")->table('item')->where('itemID', $itemID)->update(array('teamID' => $teamID, 'name' => $name, 'type' => $type, 'description' => $description, 'manufacturer' => $manufacturer, 'giveawayDate' => $giveawayDate, 'quantity' => $quantity));

			if($updateSQL)
				return true;
		}
	}

}