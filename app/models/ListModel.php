<?php

class ListModel extends Eloquent 
{
	public static function getListItems()
	{
		//$getItemsSQL = DB::connection("bobbledb")->select("SELECT name, teamID, type, description, manufacturer, giveawayDate, quantity FROM item");

		$output = array("sql"=> "", "sEcho"=> Input::get("sEcho"), "iTotalRecords"=> "", "iTotalDisplayRecords"=> "", "aaData"=> array());
		$date = $search = "";

		///////DATE LOGIC///////
		if(Input::has('specificDate'))
		     $date = "AND giveawayDate='".Input::get('specificDate')."'";
		else if(Input::has('dateStart') && Input::has('dateEnd')){
		     $date = "AND giveawayDate BETWEEN '".Input::get('dateStart')."' AND '".Input::get('dateEnd')."'";
		}

		///////SEARCH LOGIC///////
		if(Input::has("type")){
		     switch(Input::get("type")){
		          case "All":
		               $search = "AND (i.name LIKE '%".Input::get('term')."%' OR t.name LIKE '%".Input::get('term')."%' OR type LIKE '%".Input::get('term')."%' OR description LIKE '%".Input::get('term')."%' OR manufacturer LIKE '%".Input::get('term')."%') ";
		          break;

		          case "Name":
		               $search = "AND i.name LIKE '%".Input::get('term')."%'";
		          break;

		          case "Team":
		               $search = "AND t.name LIKE '%".Input::get('term')."%'";
		          break;

		          case "Type":
		               $search = "AND type LIKE '%".Input::get('term')."%'";
		          break;

		          case "Description":
		               $search = "AND description LIKE '%".Input::get('term')."%'";
		          break;

		          case "Manufacturer":
		               $search = "AND manufacturer LIKE '%".Input::get('term')."%'";
		          break;

		          default:
		               $search = "";
		          break;
		     }
		}

		$columns = array('', 'itemName', 'teamName', 'type', 'description', 'manufacturer', 'giveawayDate', 'quantity');

	     $sOrder = "";
	     if ( Input::has('iSortCol_0') )
	     {
	          $sOrder = "ORDER BY  ";
	          for ( $i=0 ; $i<intval( Input::get('iSortingCols') ) ; $i++ )
	          {
	               if ( Input::get( 'bSortable_'.intval(Input::get('iSortCol_'.$i)) ) == "true")
	               {
	                    if($columns[ intval( Input::get('iSortCol_'.$i) ) ] != "")
	                         $sOrder .= $columns[ intval( Input::get('iSortCol_'.$i) ) ]." ".mysql_real_escape_string( Input::get('sSortDir_'.$i) ) .", ";
	               }
	          }
	          
	          $sOrder = substr_replace( $sOrder, "", -2 );
	          if ( $sOrder == "ORDER BY" )
	          {
	               $sOrder = "ORDER BY giveawayDate";
	          }
	     }

	     $output["sql"] = "SELECT SQL_CALC_FOUND_ROWS t.name as teamName, i.name as itemName, type, description, manufacturer, giveawayDate, quantity, itemID, pictureExt FROM item i, team t WHERE i.teamID=t.teamID  ".$date." ".$search." ".$sOrder." ";

		$sql = DB::connection("bobbledb")->select(
			"SELECT SQL_CALC_FOUND_ROWS t.name as teamName, i.name as itemName, type, description, manufacturer, giveawayDate, quantity, i.itemID, pictureExt
			 FROM item i, team t, list l
			 WHERE i.teamID=t.teamID AND i.itemID=l.itemID AND l.userID='".Auth::user()->id."' ".$date." ".$search." AND l.deleted='0' AND i.deleted='0' ".$sOrder." ");

		$totalItems = DB::connection("bobbledb")->select("SELECT COUNT(itemID) as items FROM item");

		$output['iTotalRecords'] = $totalItems[0]->items;
		$output['iTotalDisplayRecords'] = $totalItems[0]->items;

		for($i=0; $i<count($sql); $i++)
		{
			$row = array();

			$row[] = '<button type="button" class="btn btn-danger btn-xs list" id="'.$sql[$i]->itemID.'">
		                 	<span class="glyphicon glyphicon-remove"></span>
		               </button>';
			$row[] = $sql[$i]->itemName;
			$row[] = $sql[$i]->teamName;
			$row[] = $sql[$i]->type;
			$row[] = $sql[$i]->description;
			$row[] = $sql[$i]->manufacturer;
			$row[] = $sql[$i]->giveawayDate;
			$row[] = $sql[$i]->quantity;
			$row[] = '<img src="../uploads/'.$sql[$i]->itemID.'.'.$sql[$i]->pictureExt.'" alt="'.$sql[$i]->itemName.'" height="200" width="200" />';

			$output['aaData'][] = $row;
 		}

 		return $output;
	}
}