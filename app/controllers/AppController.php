<?php

class AppController extends BaseController 
{
	public function getIndex()
	{
		return View::make('home');
	}

	public function getHome()
	{
		return View::make('home');
	}

	public function getTest()
	{
		return View::make('test');
	}

	public function getAdmin()
	{
		echo 'this is the admin home page';
	}

	public function getList()
	{
		return View::make('list');
	}

	public function getEntry()
	{
		return View::make('entry', EntryController::varsToView());
	}

	public function postEntry()
	{
		if(EntryController::save())
			return Redirect::route('entry')->with(array('responseType' => "Entry Submitted Successfully!"));
	}

	public function getTeams()
	{
		$term = Input::get('term');
		if (empty($term)) exit;
		$term = strtolower(Input::get('term'));
		if (get_magic_quotes_gpc()) $term = stripslashes($term);

		Response::json(AutoCompleteController::getTeams($term));	
	}

	public function postTeams()
	{
		echo json_encode(AutoCompleteController::getTeams());
	}

	// public function getSaveleagueinfo()
	// {
	// 	if(ParseController::save())
	// 		echo 'everything works';
	// }

	// public function postSaveleagueinfo()
	// {
	// 	if(ParseController::save())
	// 	 	echo 'this is awesome';

	// 	//print_r(json_decode(Input::get('minorTeamInfo'), true));
	// }

	public function getTable()
	{
		if($output = HomeController::getTable())
 			return Response::json($output);
	}

	public function getListfunctions()
	{
		switch (Input::get("type")) {
			case 'addList':
				HomeController::addList();
			break;
			
			case 'deleteList':
				HomeController::deleteList();
			break;

			case 'listBadge':
				return HomeController::listBadge();
			break;

			case 'markBadgesSeen':
				HomeController::markBadgesSeen();
			break;
		}
	}

	public function getEditdeletefunctions()
	{
		switch (Input::get("type")) {
			case 'edit':
			break;

			case 'delete':
				HomeController::deleteItem();
			break;
		}
	}

	public function getListtable()
	{
		if($output = ListController::getTable())
			return Response::json($output);
	}

	public function getEdit()
	{
		return View::make('entry', EntryController::editData());
	}

	public function postEdit()
	{
		if(EntryController::saveEdit())
			return Redirect::route('entry')->with(array('responseType' => "Entry Updated Successfully!"));
	}
}


