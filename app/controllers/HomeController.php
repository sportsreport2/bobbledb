<?php

class HomeController extends BaseController {

	public static function varsToView()
	{

	}

	public static function getTable()
	{
		if($output = Home::getItems())
			return $output;
	}

	public static function addList()
	{
		if(Home::addList())
			return true;
	}

	public static function deleteList()
	{
		if(Home::deleteList())
			return true;
	}

	public static function listBadge()
	{
		if(Home::listBadge())
			return true;
	}

	public static function markBadgesSeen()
	{
		if(Home::markBadgesSeen())
			return true;
	}

	public static function deleteItem()
	{
		if(Home::deleteItem())
			return true;
	}

}