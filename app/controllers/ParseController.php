<?php

class ParseController extends BaseController 
{

	public static function save()
	{
		if(ParseController::saveLeagueInfo() && ParseController::saveMinorTeamInfo())
			return true;

		// if(ParseController::saveMinorTeamInfo())
		// 	return true;
	}

	private static function saveLeagueInfo()
	{
		$jsonLeagueInfo = json_decode(Input::get("leagueInfo"), true);
		if(Parse::saveLeagueInfo($jsonLeagueInfo))
			return true;
	} 

	private static function saveMinorTeamInfo()
	{
		$jsonMinorTeamInfo = json_decode(Input::get('minorTeamInfo'), true);
		//$jsonMinorTeamInfo = array(array('name_display_full'=>'name 1', 'league_full'=>'American League', 'division_full'=>'division 1', 'sport_code_display'=>'sport code 1', 'city'=>'city1', 'state'=>'state1', 'first_year_of_play'=>'2013', 'mlb_org'=>'Milwaukee Brewers'), array('name_display_full'=>'name 2', 'league_full'=>'National League', 'division_full'=>'division 2', 'sport_code_display'=>'sport code 2', 'city'=>'city2', 'state'=>'state2', 'first_year_of_play'=>'2010', 'mlb_org'=>'Minnesota Twins'));
		//$jsonMinorTeamInfo = array(array('name_display_full' => 'American League All-Stars', 'league_full' => 'American League', 'division_full' => '', 'sport_code_display' => 'Major League Baseball', 'city' => '', 'state' => '','first_year_of_play' => '1933', 'mlb_org' => ''), array('name_display_full' => 'Baltimore Orioles', 'league_full' => 'American League', 'division_full' => 'American League East', 'sport_code_display' => 'Major League Baseball', 'city' => 'Baltimore', 'state' => 'MD', 'first_year_of_play' => '1954', 'mlb_org' => '')); 
		// if(Parse::saveMinorTeamInfo($jsonMinorTeamInfo))
		// 	return true;

		return Parse::saveMinorTeamInfo($jsonMinorTeamInfo);
	}

}