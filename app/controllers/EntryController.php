<?php

class EntryController extends BaseController 
{

	public static function varsToView()
	{
		$vars['teams'] = Entry::getTeams();

		return $vars; 
	}

	public static function editData()
	{
		$data = Entry::editData();

		$vars['itemID'] = $data[0]->itemID;
		$vars['itemName'] = $data[0]->itemName;
		$vars['type'] = $data[0]->type;
		$vars['description'] = $data[0]->description;
		$vars['teamID'] = $data[0]->teamID;
		$vars['manufacturer'] = $data[0]->manufacturer;
		$vars['giveawayDate'] = $data[0]->giveawayDate;
		$vars['quantity'] = $data[0]->quantity;
		$vars['picture'] = $data[0]->itemID.'.'.$data[0]->pictureExt;
		$vars['teams'] = Entry::getTeams();

		return $vars;
	}

	public static function save()
	{
		if(EntryController::saveEntryInfo())
			return true;
	}

	public static function saveEntryInfo()
	{
		$teamID = Input::get("team");
		$name = Input::get("name");
		$type = Input::get("type");
		$description = Input::get("description");
		$manufacturer = Input::get("manufacturer");
		$giveawayDate = Input::get("giveawayDate");
		$quantity = Input::get("quantity");
		$picture = Input::file("picture");

		if(Entry::saveEntryInfo($teamID, $name, $type, $description, $manufacturer, $giveawayDate, $quantity, $picture))
			return true;
	} 

	public static function saveEdit()
	{
		$itemID = Input::get("itemID");
		$teamID = Input::get("team");
		$name = Input::get("name");
		$type = Input::get("type");
		$description = Input::get("description");
		$manufacturer = Input::get("manufacturer");
		$giveawayDate = Input::get("giveawayDate");
		$quantity = Input::get("quantity");
		$picture = Input::file("picture");

		if(Entry::saveEdit($itemID, $teamID, $name, $type, $description, $manufacturer, $giveawayDate, $quantity, $picture))
			return true;
	}

}