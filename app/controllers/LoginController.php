<?php

class LoginController extends BaseController 
{

	public function getIndex()
	{
		return View::make('login');
	}

	public function getLogin()
	{
		return View::make('login');
	}

	public function postLogin()
	{
		$userdata = array (
			'email' => Input::get('email'),
			'password' => Input::get('password'));
		if(Auth::attempt($userdata))
		{
			// echo 'successful';
			// echo 'EMAIL: '.Auth::user()->email;
			
			//echo 'login successful';

			//print_r(Auth::user());

			return Redirect::action('AppController@getHome');
		}else{
			echo 'Try Again';
		}
	}

	public function getRegister()
	{
		return View::make('register');
	}

	public function postRegister()
	{
		$validator = Validator::make(Input::all(), User::$rules);

		if ($validator->passes()) 
		{
		    	$user = new User;
			$user->firstname = Input::get('firstname');
			$user->lastname = Input::get('lastname');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->save();

			return Redirect::route('login')->with('message', 'Thanks for registering!');
		} else 
		{
		    	return Redirect::route('register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput(); 
		}
	}

	public function getLogout()
	{
		Auth::logout();
		//print_r(Auth::user());
		return View::make('login');
	}

	// public function getStart()
	// {
	// 	$user = new Role();
	// 	$user->name = 'User';
	// 	$user->save();

	// 	$admin = new Role();
	// 	$admin->name = 'Admin';
	// 	$admin->save();

	// 	$manageUsers = new Permission();
	// 	$manageUsers->name = 'can_manage_users';
	// 	$manageUsers->display_name = 'Can Manage Users';
	// 	$manageUsers->save();

	// 	$admin->attachPermission($manageUsers);

	// 	$user1 = User::find(1);
	// 	$user1->attachRole(2);
	// }

	// public function getTest()
	// {
	// 	return View::make('parse.parse');
	// }
}

