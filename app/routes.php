<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function(){
	Route::controller('app', 'AppController', array(
		'getEntry' => 'entry',
		'getEdit' => 'edit'));
});

Route::controller('/', 'LoginController', array(
	'getRegister' => 'register',
	'getLogin' => 'login',
	'getHome' => 'home'
));

// Route::get("/start", function(){

// });

Entrust::routeNeedsRole('app/admin*', 'Admin');