<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bobbledb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('firstname', 20);
			$table->string('lastname', 20);
			$table->string('email', 100)->unique();
			$table->string('password', 64);
			$table->boolean('paid');
			$table->timestamps();
		});

		Schema::create('league', function($table){
			$table->increments('leagueID')->unsigned();
			$table->string('nameAbbrev');
			$table->string('sportcode');
			$table->string('nameFull');
			$table->string('sportCodeDisplay');
			$table->boolean('active');
			$table->timestamps();
		});

		Schema::create('team', function($table){
			$table->increments('teamID')->unsigned();
			$table->integer('leagueID')->unsigned();
			$table->foreign('leagueID')->references('leagueID')->on('league');
			$table->integer('mlbOrgID')->unsigned();
			$table->string('mlbOrgAbbrev');
			$table->string('city');
			$table->string('state');
			$table->string('sportCode');
			$table->string('sportCodeDisplay');
			$table->string('displayNameFull');
			$table->string('displayNameBrief');
			$table->boolean('active');
			$table->timestamps();
		});

		Schema::create('item', function($table){
			$table->increments('itemID')->unsigned();
			$table->integer('teamID')->unsigned();
			$table->foreign('teamID')->references('teamID')->on('team');
			$table->string('name');
			$table->string('type');
			$table->string('manufacturer');
			$table->date('giveawayDate');
			$table->integer('quantity')->unsigned();
			$table->binary('picture');
			$table->integer('createdBy')->unsigned();
			$table->foreign('createdBy')->references('id')->on('users');
			$table->timestamps();
		});

		Schema::create('list', function($table){
			$table->increments('listID')->unsigned();
			$table->integer('userID')->unsigned();
			$table->foreign('userID')->references('id')->on('users');
			$table->integer('itemID')->unsigned();
			$table->foreign('itemID')->references('itemID')->on('item');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Schema::table('team', function(Blueprint $table){
		// 	$table->dropForeign('')
		// });
	}

}
