///////SEARCH FUNCTIONS///////
function selectSearchType(value){
     $("#search").html(value+" <span class=\"caret\"></span>");
}

function reloadTableSearch(type, term){
     displayFilterString();
     searchParams = [];
     searchParams.push({"name":"type", "value":type.trim()});
     searchParams.push({"name":"term", "value":term.trim()});
     storeParams();
}

///////GLOBAL FUNCTIONS///////
function storeParams(){
     parameters = [];
     for(var i=0; i<dateParams.length; i++)
          parameters.push(dateParams[i]);

     for(i=0; i<searchParams.length; i++)
          parameters.push(searchParams[i]);
     filter = true;
     table.fnDraw();
}

function displayFilterString(){
     filterString = "";

     if($("#dateRange").text() == "Date Range"){
          if(month!=-1 && day==0)
               filterString = monthNames[month]+" "+numberSuffix(1)+" - "+numberSuffix(numberOfDays(year, month));
          else if(month!=-1 && day!=0)
               filterString = monthNames[month]+" "+numberSuffix(day);
          else
               filterString = "";
     }else{
          if(month==monthRange && day==0 && dayRange==0)
               filterString = monthNames[month]+" "+numberSuffix(1)+" - "+numberSuffix(numberOfDays(year, month));
          else if(month==monthRange && day!=0 && dayRange==0)
               filterString = monthNames[month]+" "+numberSuffix(day)+" - "+numberSuffix(numberOfDays(year, month));
          else if(month==monthRange && day==0 && dayRange!=0)
               filterString = monthNames[month]+" "+numberSuffix(1)+" - "+numberSuffix(dayRange);
          else if(month==monthRange && day!=0 && dayRange!=0)
               filterString = monthNames[month]+" "+numberSuffix(day)+" - "+numberSuffix(dayRange);
          else if(month!=monthRange && day==0 && dayRange==0)
               filterString = monthNames[month]+" "+numberSuffix(1)+" - "+monthNames[monthRange]+" "+numberSuffix(numberOfDays(year, monthRange));
          else if(month!=monthRange && day!=0 && dayRange==0)
               filterString = monthNames[month]+" "+numberSuffix(day)+" - "+monthNames[monthRange]+" "+numberSuffix(numberOfDays(year, monthRange));
          else if(month!=monthRange && day==0 && dayRange!=0)
               filterString = monthNames[month]+" "+numberSuffix(1)+" - "+monthNames[monthRange]+" "+numberSuffix(dayRange);
          else if(month!=monthRange && day!=0 && dayRange!=0)
               filterString = monthNames[month]+" "+numberSuffix(day)+" - "+monthNames[monthRange]+" "+numberSuffix(dayRange);
          else
               filterString = "";
     }

     if($("#searchText").val() != ""){
          if(filterString != "")
               filterString += "; Rows containing \""+$("#searchText").val()+"\" in ";
          else
               filterString += "Rows containing \""+$("#searchText").val()+"\" in ";

          if($("#search").text().trim() == "All")
               filterString += $("#search").text().toLowerCase()+" columns";
          else
               filterString += $("#search").text().toLowerCase()+" column";
     }

     $("#filterText").html("<strong>Filters: </strong>"+filterString);
}

function numberSuffix(i) {
     var j = i % 10;
     if (j == 1 && i != 11) {
          return i + "st";
     }
     if (j == 2 && i != 12) {
          return i + "nd";
     }
     if (j == 3 && i != 13) {
          return i + "rd";
     }
     return i + "th";
}

///////DATE FUNCTIONS///////
function selectMonthRange(value){
     if(value == "Month")
          monthRange = -1;
     else
          monthRange = monthNames.indexOf(value);

     populateDates("rangeDay", monthRange);
     $("#rangeMonth").html(value+" <span class=\"caret\"></span>");
}

function selectDayRange(value){
     if(value == "Day")
          dayRange = 0;
     else
          dayRange = parseInt(value);
     $("#rangeDayBtn").html(value+" <span class=\"caret\"></span>");
}

function resetRangeDate(){
     $(".rangeGroup").hide();
     $("#dateRange").removeClass("btn-danger").addClass("btn-warning").text("Date Range");
     selectMonthRange("Month");
     selectDayRange("Day");
}

function resetDate(){
     // $("#dayGroup").hide();
     // selectMonth("Month");
     // selectDay("Day");

     $("#fromDate").val("");
     $("#toDate").val("");
     $("#fromDate").datepicker('option', {minDate: null, maxDate: null});
     $("#toDate").datepicker('option', {minDate: null, maxDate: null});
}

function numberOfDays(year, month) {
     var d = new Date(year, month+1, 0);
     return d.getDate();
}

function selectMonth(value){
     if(value == "Month"){
          month = -1;
          $(".dateRange").hide();
     }else{
          month = monthNames.indexOf(value);
          $(".dateRange").show();
     }
     populateDates("day", month);
     $("#month").html(value+" <span class=\"caret\"></span>");
}

function selectDay(value){
     if(value == "Day")
          day = 0;
     else
          day = parseInt(value);
     $("#dayBtn").html(value+" <span class=\"caret\"></span>");
}

function populateDates(className, month){
     var d = new Date();
     var days = "";
     for(i=1; i<=numberOfDays(d.getFullYear(), month); i++){
          days += "<li><a class=\""+className+"\" id=\""+className+i+"\" href=\"#\">"+i+"</a></li>";
     }
     $("#"+className).html(days);
}

function reloadTableDate(month, day){
     disableRangeOptions();
     displayFilterString();
     dateParams = [{"name":"specificDate", "value":year+"-"+('0'+(month+1)).slice(-2)+"-"+('0'+day).slice(-2)}];
     storeParams();
}

function reloadTable(dateStart, dateEnd){
     disableRangeOptions();
     displayFilterString();
     dateParams = [];

     if($("#dateRange").text() == "Date Range"){
          if(day == 0){
               dateStart = year+"-"+('0'+(month+1)).slice(-2)+"-01";
               dateEnd = year+"-"+('0'+(month+1)).slice(-2)+"-"+numberOfDays(year, month);
          }else{
               reloadTableDate(month, day);
               return;
          }
     }else{
          if(dayRange == 0){
               var dateStart = year+"-"+('0'+(month+1)).slice(-2)+"-"+('0'+(day)).slice(-2);
               var dateEnd = year+"-"+('0'+(monthRange+1)).slice(-2)+"-"+numberOfDays(year, monthRange);
          }else{
               var dateStart = year+"-"+('0'+(month+1)).slice(-2)+"-"+('0'+(day)).slice(-2);
               var dateEnd = year+"-"+('0'+(monthRange+1)).slice(-2)+"-"+('0'+(dayRange)).slice(-2);
          }
     }

     dateParams.push({"name":"dateStart", "value":dateStart});
     dateParams.push({"name":"dateEnd", "value":dateEnd});
     storeParams();
}

function disableRangeOptions(){
     $(".rangeMonth, .rangeDay").removeClass("disabledLink").parent().removeClass("disabled");
    
     if(month > monthRange){
          selectMonthRange("Month");
          selectDayRange("Day");
          $("#rangeDayGroup").hide();
     }

     if(day > dayRange && month==monthRange){
          selectDayRange("Day");
     }

     for(var i=month-1; i>=0; i--){
          $("#"+monthNames[i]).addClass("disabledLink").parent().addClass("disabled");
     }

     if(day != 0 && month==monthRange){
          for(i=day; i>0; i--){
               $("#rangeDay"+i).addClass("disabledLink").parent().addClass("disabled");
          }
     }
}