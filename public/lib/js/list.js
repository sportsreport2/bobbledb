'use strict';

var table = "";
var parameters = [];
var filter = false;

var oCache = {
    iCacheLower: -1
};

function fnSetKey( aoData, sKey, mValue )
{
    for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
    {
        if ( aoData[i].name == sKey )
        {
            aoData[i].value = mValue;
        }
    }
}

function fnGetKey( aoData, sKey )
{
    for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
    {
        if ( aoData[i].name == sKey )
        {
            return aoData[i].value;
        }
    }
    return null;
}

function fnDataTablesPipeline ( sSource, aoData, fnCallback ) {
    var iPipe = 5; /* Ajust the pipe size */
    
    var json = "";
    var bNeedServer = false;
    var sEcho = fnGetKey(aoData, "sEcho");
    var iRequestStart = fnGetKey(aoData, "iDisplayStart");
    var iRequestLength = fnGetKey(aoData, "iDisplayLength");
    var iRequestEnd = iRequestStart + iRequestLength;
    oCache.iDisplayStart = iRequestStart;
    
    /* outside pipeline? */
    if ( oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper || filter)
    {
        bNeedServer = true;
        filter = false;
    }
    
    /* sorting etc changed? */
    if ( oCache.lastRequest && !bNeedServer )
    {
        for( var i=0, iLen=aoData.length ; i<iLen ; i++ )
        {
            if ( aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho" )
            {
                if ( aoData[i].value != oCache.lastRequest[i].value )
                {
                    bNeedServer = true;
                    break;
                }
            }
        }
    }
    
    /* Store the request for checking next time around */
    oCache.lastRequest = aoData.slice();
    
    if ( bNeedServer )
    {
        if ( iRequestStart < oCache.iCacheLower )
        {
            iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
            if ( iRequestStart < 0 )
            {
                iRequestStart = 0;
            }
        }
        
        oCache.iCacheLower = iRequestStart;
        oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
        oCache.iDisplayLength = fnGetKey( aoData, "iDisplayLength" );
        fnSetKey( aoData, "iDisplayStart", iRequestStart );
        fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe );

        
        /* Add some extra data to the sender */
        if(parameters.length >= 1){
            for(var i=0; i<parameters.length; i++){
                aoData.push( parameters[i] );
            }
        }

        $.getJSON( sSource, aoData, function (json) { 
            /* Callback processing */
            oCache.lastJson = jQuery.extend(true, {}, json);
            
            if ( oCache.iCacheLower != oCache.iDisplayStart )
            {
                json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower );
            }
            json.aaData.splice( oCache.iDisplayLength, json.aaData.length );
            
            //alert(JSON.stringify(json));
            fnCallback(json)
        } );
    }
    else
    {
        json = jQuery.extend(true, {}, oCache.lastJson);
        json.sEcho = sEcho; /* Update the echo for each response */
        json.aaData.splice( 0, iRequestStart-oCache.iCacheLower );
        json.aaData.splice( iRequestLength, json.aaData.length );
        //alert(JSON.stringify(json));
        fnCallback(json);
        return;
    }
}

$(document).ready(function () {
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480,
        desktop: 1000000
    };
    var tableElement = $('#example');

    table = tableElement.dataTable({
        //sDom           : '<"container-fluid"<"row"<"col-xs-12"p><"col-md-12">r<"centered"i>t<"row"<"col-md-12"><"col-md-12">>>',
        sDom:'pirt',
        sPaginationType: 'simple_numbers',
        oLanguage      : {
            sLengthMenu: '_MENU_ records per page',
        },
        "aaSorting": [[ 6, "asc" ]],
        bjQueryUI      : true,
        bProcessing    : true,
        bAutoWidth     : false,
        sAjaxSource    : 'listtable',
        bDeferRender   : true,
        bServerSide    : true,
        fnServerData: fnDataTablesPipeline,
        // fnPreDrawCallback: function () {
        //     // Initialize the responsive datatables helper once.
        //     if (!responsiveHelper) {
        //         responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
        //     }
        // },
        // fnRowCallback  : function (nRow) {
        //     responsiveHelper.createExpandIcon(nRow);
        // },
        // fnDrawCallback : function (oSettings) {
        //     responsiveHelper.respond();
        // }
    });
});
