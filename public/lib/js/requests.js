'use strict';

var table = "";
var parameters = [];

$(document).ready(function () {
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480,
        desktop: 1000000
    };
    var tableElement = $('#requests');

    table = tableElement.dataTable({
        //sDom           : '<"container-fluid"<"row"<"col-xs-12"p><"col-md-12">r<"centered"i>t<"row"<"col-md-12"><"col-md-12">>>',
        sDom:'pirt',
        sPaginationType: 'bootstrap',
        oLanguage      : {
            sLengthMenu: '_MENU_ records per page',
            oPaginate:{
                "sNext":"",
                "sPrevious":""
            }
        },
        bProcessing    : true,
        bAutoWidth     : false,
        sAjaxSource    : './requests.php',
        fnServerData: function ( sSource, aoData, fnCallback ) {
            /* Add some extra data to the sender */
            if(parameters.length >= 1){
                for(var i=0; i<parameters.length; i++){
                    aoData.push( parameters[i] );
                }
            }
            $.getJSON( sSource, aoData, function (json) { 
                /* Do whatever additional processing you want on the callback, then tell DataTables */
                fnCallback(json)
            } );
        },
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
});
