     ///////DEFINE VARIABLES///////
     var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
     var month = -1;
     var monthRange = -1;
     var day = 0;
     var dayRange = 0;
     var year = "2014";
     var dateParams = [];
     var searchParams = [];

$(function(){
     ///////SEARCH PROCESS///////
     $(document).on("click", ".search", function(){
          selectSearchType($(this).text());
          $("#searchText").focus();
          reloadTableSearch($(this).text(), $("#searchText").val());
     });

     $(document).on("keyup", "#searchText", function(){
          reloadTableSearch($("#search").text(), $(this).val());
     });

     ///////DATE PROCESS///////
     $("#dayGroup, .dateRange, .rangeGroup").hide();

     //original date or from date in date range
     $(document).on("click", ".month", function(){
          prevMonth = month;
          selectMonth($(this).text()); 
           
          if(dayRange != 0){
               if(day == numberOfDays(year, prevMonth))
                    selectDay(numberOfDays(year, month))
          }
          reloadTable();      
          
          $("#dayGroup").show();
     });

     $(document).on("click", ".day", function(){
          selectDay($(this).text());
          reloadTable();
     });

     $(document).on("click", "#dayX", function(){
          selectDay("Day");
          reloadTable();
     });

     $(document).on("click", "#today", function(){
          resetRangeDate();
          $("#dayX").show();
          var d = new Date();
          selectMonth(monthNames[d.getMonth()]);
          selectDay(d.getDate());
          $("#dayGroup").show();
          reloadTableDate(('0'+(d.getMonth()+1)).slice(-2), ('0'+(d.getDate())).slice(-2));
     });

     $(document).on("click", "#reset", function(){
          resetDate();
          resetRangeDate();
          displayFilterString();
          dateParams = [];
          storeParams();
     });

     $(document).on("click", "#dateRange", function(){
          if($(this).text() == "Date Range"){
               $("#rangeMonthGroup").show();
               $(this).removeClass("btn-warning").addClass("btn-danger").text("Clear Range");
          }else{
               resetRangeDate();
               reloadTable();
          }

     });

     //to date in date range
     $(document).on("click", ".rangeMonth", function(){
          prevMonth = monthRange;
          selectMonthRange($(this).text()); 

          if(dayRange != 0){     
               if(dayRange == numberOfDays(year, prevMonth))
                    selectDayRange(numberOfDays(year, monthRange)) 
          }
          reloadTable();             
          
          $("#rangeDayGroup").show();
     });

     $(document).on("click", ".rangeDay", function(){
          selectDayRange($(this).text());
          reloadTable();
     });

     $(document).on("click", "#rangeDayX", function(){
          selectDayRange("Day");
          reloadTable();
     });
});
